package pl.codementors.mvc3;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class User {
    @NotNull
    private Long id;
    @NotNull
    @JsonProperty("user_name")
    private String username;
    @NotNull
    @JsonProperty("first_name")
    private String firstname;
    @NotNull
    @JsonProperty("last_name")
    private String lastname;
    @NotNull
    @Min(value = 1)
    @Max(value = 130)
    private Integer age;

    public User(){

    }

    public User(Long id, String username, String firstname, String lastname, Integer age) {

        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User id= " + id;
    }
}
