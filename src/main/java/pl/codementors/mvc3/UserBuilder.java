package pl.codementors.mvc3;

public class UserBuilder {

    private Long id;
    private String username;
    private String firstname;
    private String lastname;
    private Integer age;

    public UserBuilder withId(Long id){
        this.id = id;
        return this;
    }

    public UserBuilder withUserName(String userName){
        this.username = userName;
        return this;
    }

    public UserBuilder withFirstName(String firstName){
        this.firstname = firstName;
        return this;
    }

    public UserBuilder withLastName(String lastName){
        this.lastname = lastName;
        return this;
    }

    public UserBuilder withAge(Integer age){
        this.age = age;
        return this;
    }

    public User build(){
        User u = new User();
        u.setId(this.id);
        u.setUsername(this.username);
        u.setFirstname(this.firstname);
        u.setLastname(this.lastname);
        u.setAge(this.age);
        return u;
    }
}
