package pl.codementors.mvc3;

import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@org.springframework.context.annotation.Configuration
public class MyConfiguration extends WebMvcConfigurerAdapter {

//  do springboota 2 tak to należy rozwiązywać
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
//        converters.add(jacksonConverter);
//    }

    @Bean
    public HttpMessageConverters customConverters() {
        MappingJackson2HttpMessageConverter jacksonHttpConverter = new MappingJackson2HttpMessageConverter();
        MappingJackson2XmlHttpMessageConverter jackson2XmlHttpMessageConverter = new MappingJackson2XmlHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(jacksonHttpConverter, jackson2XmlHttpMessageConverter);
        return converters;
    }
}
