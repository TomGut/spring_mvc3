package pl.codementors.mvc3;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    protected final Logger log = Logger.getLogger(getClass().getName());
    private List<User> users = new ArrayList<>();

    @RequestMapping(value = "/home/{id}", method = RequestMethod.GET)
    public User createUser(
            @PathVariable("id") Long id) {
        UserBuilder builder = new UserBuilder();
        builder.withId(id);
        return builder.build();
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String userId(@RequestBody @Valid User user) {
        UserBuilder builder = new UserBuilder();
        builder.withId(user.getId());
        builder.withUserName(user.getUsername());
        builder.withAge(user.getAge());
        builder.withFirstName(user.getFirstname());
        builder.withLastName(user.getLastname());
        users.add(builder.build());
        log.log(Level.INFO, "Powiodło się, user ma id = ", user.getId().toString());
        return user.toString();
    }

    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public List getAllUsers(){
        return users;
    }
}
